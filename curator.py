#!/usr/bin/python3

def do_help():
    print ("""

Usage:
curator poll
curator git
curator email
curator prune
curator serve

Each of these runs an internal loop, so run them from systemd.

curator poll - polls the patchwork server for new events and stores
               them in the database.

curator git - does a "git pull" on the source tree looking for new
              commits.

curator email - imports new email messages and looks for CICD commands.

curator prune - removes old events from the database.

curator cgi - acts as a CGI script for apache/httpd.  The URL at
              which this appears, is what the Runners need to know.

curator http - listens on curatorPort, ignores URL, provides JSON.
""")

# If you're still reading these comments and want to "follow the
# code", jump to the bottom of this file :-)

# Design goals:

# resource leaks - must be able to run "forever"

# should be killable at any time

# Don't hold locks or open files while waiting between iterations

# One source file (plus config), only standard python modules

# Note: SQL parameter lists *must* include at least one comma, else
# they're not lists.

#------------------------------------------------------------------

import io
import os
import sys
import time
import datetime
import random
import gnupg
import mariadb
import requests
import json
import http.server
import socketserver
import urllib
import pygit2
import email
import inspect

import warnings
with warnings.catch_warnings():
    warnings.filterwarnings("ignore",category=DeprecationWarning)
    import cgi
    import cgitb

#------------------------------------------------------------------
# Generic timed-loop logic.  Pass min/max times and function to call.
# Function should return true if "something happened", which causes
# the poll time to be reset to minimum.

def do_timed_loop(minTime, maxTime, theFunction):
    curTime = minTime
    while True:
        if theFunction():
            curTime = minTime
        else:
            curTime = min (curTime * 2, maxTime)
        #print ("sleep ", curTime)
        time.sleep (curTime)

#------------------------------------------------------------------
# Database connections

def open_database ():
    global db_conn
    global db_cur
    try:
        db_conn = mariadb.connect(
            user = curatorDBUser,
            password = curatorDBPassword,
            database = curatorDBName,
            host = '127.0.0.1')
    except mariadb.Error as err:
        print (err, file=sys.stderr)
        return False

    db_cur = db_conn.cursor ()
    return True

def close_database ():
    db_conn.close ()

# variables' values are strings

def db_get_variable (var):
    db_cur.execute("SELECT value FROM state WHERE variable = ?;", (var,))
    ret = db_cur.fetchall()
    # return the first field of the first row returned
    if len (ret) > 0:
        return ret[0][0]
    return False

def db_set_variable (var, value):
    db_cur.execute("insert into state values (?, ?) on duplicate key update value=?;",
                   (var, value, value))
    db_conn.commit()
    return False

#------------------------------------------------------------------
# poll

def do_poll_iter ():
    if not open_database ():
        return False

    # We calculate our new "last poll" time before polling, as a bit
    # of overlap is better than a bit of gap and potential lost
    # events.  We don't *save* the new last poll until after the poll
    # is successful.

    now = datetime.datetime.utcnow()
    last_poll = db_get_variable ("last-event-poll")
    if (last_poll == False):
        # The most we want the first time, depends on how old "expired" events are
        last_poll = now - datetime.timedelta (seconds = eventExpireTime)
    else:
        last_poll = datetime.datetime.fromisoformat (last_poll)


    # The per_page is arbitrarily large so we don't have to deal with
    # multi-page requests.
    q = {'since': last_poll.isoformat (),
         'project': patchworkProject,
         'per_page': '2000'}

    # Fetch all events since "last_poll" and insert any new ones into
    # the database.

    got_a_new_event = False

    # We reset the wait time to minimum on *any* event, not just the
    # ones we record, as "something happened" implies "something we
    # care about might happen soon."

    try:
        response = requests.get (patchworkURL + '/api/1.3/events/', params=q)
    except:
        # network error, don't reset timeouts
        return False;
    if response.status_code == 200:
        for event in response.json ():
            got_a_new_event = True

            if event['category'] == "series-completed":

                # get the JSON data for the series, not the event
                try:
                    sresp = requests.get (patchworkURL + '/api/1.2/series/'
                                          + str(event['payload']['series']['id']) + '/')
                except:
                    # network error, don't reset timeouts
                    return False;
                if sresp.status_code == 200:
                    payload = sresp.text
                else:
                    payload = json.dumps (event)

                # Insert if it isn't already there
                db_cur.execute ("""INSERT INTO events (edate, type, series_id, json)
                                   SELECT ?,?,?,? WHERE (SELECT COUNT(*)
                                                       FROM events
                                                       WHERE type = 'PATCH' and series_id = ?) = 0""",
                                (event['date'],
                                 "PATCH",
                                 event['payload']['series']['id'],
                                 payload,
                                 event['payload']['series']['id']
                                 ))

            if event['category'] == "patch-comment-created":
                # but replace the placeholders with full info
                try:
                    sresp = requests.get (event['payload']['comment']['url'])
                except:
                    # network error, don't reset timeouts
                    return False;
                if sresp.status_code == 200:
                    event['payload']['comment'] = sresp.json ()
                else:
                    continue;

                # check for GPG signature here
                authuser = ''
                command = ''
                gpg = gnupg.GPG()
                gpg.encoding = 'utf-8'
                d = gpg.decrypt(event['payload']['comment']['content']);
                if (d.username == None):
                    continue;
                authuser = d.username
                # canonicalize all whitespace
                words = str(d).split()
                if (words[0] != '%cicd'):
                    continue;
                command = " ".join(words)

                try:
                    sresp = requests.get (event['payload']['patch']['url'])
                except:
                    # network error, don't reset timeouts
                    return False;
                if sresp.status_code == 200:
                    event['payload']['patch'] = sresp.json ()
                    series_id = event['payload']['patch']['series'][0]['id']
                else:
                    # best we can do
                    series_id = event['payload']['patch']['id']
                

                payload = event['payload'];
                hash = payload['comment']['msgid'];
                db_cur.execute("SELECT hash FROM events WHERE type = 'COMMAND' AND authorized_by = ? AND hash = ?",
                               (authuser, hash))
                results = db_cur.fetchall ()
                if len (results) > 0:
                    continue

                db_cur.execute("""INSERT INTO events (edate, type, series_id, json, authorized_by, command, hash)
                                  VALUES (?, ?, ?, ?, ?, ?, ?)""",
                               (event['date'], 'COMMAND', series_id, json.dumps(payload),
                                authuser, command, hash))

    db_conn.commit ()
    db_set_variable ("last-event-poll", now.isoformat());
    close_database ()
    return got_a_new_event

def do_poll_loop ():
    do_timed_loop (minPWPoll, maxPWPoll, do_poll_iter)

#------------------------------------------------------------------

def do_git ():
    print ("running git...");
    rv = os.spawnlp (os.P_WAIT, 'git',
                     'git', '-C', gitDir, 'pull', '-q', '--rebase');

    if not open_database ():
        return False

    repo = pygit2.Repository (gitDir)
    oldest = datetime.datetime.utcnow() - datetime.timedelta (seconds = eventExpireTime);

    last = repo[repo.head.target]
    saw_new_commit = False;
    for commit in repo.walk(last.id, pygit2.GIT_SORT_TIME):
        hash = commit.id;

        ct = datetime.datetime.utcfromtimestamp(commit.commit_time)
        if ct < oldest:
            break

        db_cur.execute("SELECT hash FROM events WHERE hash = ? AND type = 'COMMIT'",
                       (hash.__str__ (),))
        results = db_cur.fetchall ()
        if len (results) > 0:
            break

        print ("git hash ", hash);
        db_cur.execute("INSERT INTO events (edate, type, hash) SELECT ?,'COMMIT',?",
                       (ct, hash.__str__ ()))
        saw_new_commit = True

    db_conn.commit ()
    close_database ()
    return saw_new_commit;

def do_git_loop ():
    do_timed_loop (minGitPoll, maxGitPoll, do_git)

#------------------------------------------------------------------

# Assumes one incoming email on stdin
def do_email ():
    mail = email.parser.Parser().parse (sys.stdin);
    print (mail['in-reply-to']);
    print (type(mail))
    tl = inspect.getmembers (mail, predicate=inspect.ismethod)
    for t in tl:
        print (t[0]);
    print (mail.is_multipart());
    print (mail.get_payload());
    exit (0);

    b = mail.find("-----BEGIN PGP SIGNED MESSAGE-----")
    if not b:
        exit (0)
    mail = mail[b:]
    end = "-----END PGP SIGNATURE-----"
    e = mail.find(end)
    if not e:
        exit (0)
    e += len (end)
    mail = mail[:e]

    gpg = gnupg.GPG()
    v = gpg.verify(mail)

    if not v:
        exit (0)
    if v.trust_level == TRUST_UNDEFINED or v.trust_level == TRUST_NEVER:
        exit (0)

    if not open_database ():
        return False

    for line in email.body().splitlines():
        tag = find (line, "%cicd")
        if tag > 0:
            tag += 5
            words = line[tag:].split()
            for word in words:
                pass

#------------------------------------------------------------------

def do_prune ():
    if not open_database ():
        return False
    now = datetime.datetime.utcnow()
    expire_period = datetime.timedelta (seconds = eventExpireTime);
    expire_moment = now - expire_period;
    db_cur.execute("DELETE FROM events WHERE edate < ?",
                   (expire_moment.isoformat(), ));
    db_conn.commit ()
    close_database ()

def do_prune_loop ():
    do_timed_loop (eventExpirePollTime, eventExpirePollTime, do_prune)

#------------------------------------------------------------------

class ByteEncoder(json.JSONEncoder):
    def default(self, x):
        return x.decode('utf-8') if isinstance(x, bytes) else super().default(x)

def do_cgi_common (since):
    if not open_database ():
        return False

    events = [];
    db_cur.execute ("SELECT * FROM events WHERE edate >= ? ORDER BY edate", (since,));
    results = db_cur.fetchall ()
    for r in results:
        event = {}
        event['date'] = r[0].isoformat ()
        event['type'] = r[1]
        if r[2]:
            event['mbox_url'] = patchworkURL + "series/" + str (r[2]) + "/mbox/";
            event['series_id'] = r[2]
        if r[3]:
            event['authorized_by'] = r[3]
        if r[4]:
            event['command'] = r[4]
        if r[5]:
            event['hash'] = r[5]
        if r[6]:
            event['data'] = json.loads(r[6]);
        events.append (event)

    close_database ()
    return json.dumps (events, indent=2, cls=ByteEncoder)

def do_cgi ():
    cgitb.enable()
    form = cgi.FieldStorage();

    print ("Status: 200")
    print ("Content-type: text/json")
    print ("")

    try :
        since = datetime.datetime.fromisoformat (form.getvalue('since'))
    except:
        since = datetime.datetime.utcnow() - datetime.timedelta (seconds = eventExpireTime)

    o = do_cgi_common (since)
    print (o)

class http_handler (http.server.BaseHTTPRequestHandler):
    def do_GET (self):
        url = urllib.parse.urlparse(self.path)
        h = self.headers;
        defsince = datetime.datetime.utcnow() - datetime.timedelta (seconds = eventExpireTime)
        if url.query.startswith ("since="):
            try:
                since = datetime.datetime.fromisoformat (url.query[6:]);
            except:
                since = defsince
        else:
            since = defsince

        self.wfile.write (bytes ("HTTP/1.0 200 OK\n", 'ascii'))
        self.wfile.write (bytes ("Content-type: text/json\n\n", 'ascii'))

        o = do_cgi_common (since)
        self.wfile.write (bytes (o, 'ascii'))

def do_http ():
    socketserver.TCPServer.allow_reuse_address = True
    serv = socketserver.TCPServer (('', curatorPort), http_handler)
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        serv.shutdown ()
        serv.socket.close ()

#------------------------------------------------------------------
# This is the main starting point for this file.

# find and read the config file.
mypath = os.path.realpath (__file__)
mydir = os.path.dirname (mypath)
myconfig = mydir + "/cicd-config.py"
exec (compile (filename=myconfig, source=open (myconfig).read (), mode="exec"))

if not patchworkURL.endswith('/'):
    patchworkURL += '/'

if len (sys.argv) < 2:
    do_help ()
    exit (1)

if sys.argv[1] == "poll":
    do_poll_loop ()

elif sys.argv[1] == "git":
    do_git_loop ()

elif sys.argv[1] == "email":
    do_email ()

elif sys.argv[1] == "prune":
    do_prune_loop ()

elif sys.argv[1] == "cgi":
    do_cgi ()

elif sys.argv[1] == "http":
    do_http ()

# So you can just symlink the curator in to the cgi-bin directory
elif os.environ.get('REQUEST_METHOD'):
    do_cgi ()

else:
    do_help ()
