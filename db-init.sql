CREATE TABLE IF NOT EXISTS state (
	variable VARCHAR(20) PRIMARY KEY,
	value VARCHAR(40)
) character set 'utf8mb4' collate 'utf8mb4_bin';

CREATE TABLE IF NOT EXISTS events (
	edate DATETIME,
	type ENUM('PATCH', 'COMMAND', 'COMMIT'),
	series_id INTEGER,
	authorized_by VARCHAR(40),
	command VARCHAR(40),
	hash VARCHAR(42),
	json LONGTEXT
) character set 'utf8mb4' collate 'utf8mb4_bin';

CREATE TABLE IF NOT EXISTS runner (
	edate DATETIME,
	type ENUM('PATCH', 'COMMAND', 'COMMIT'),
	series_id INTEGER,
	authorized_by VARCHAR(40),
	command VARCHAR(40),
	hash VARCHAR(42)
) character set 'utf8mb4' collate 'utf8mb4_bin';
