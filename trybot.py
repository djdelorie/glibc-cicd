#!/usr/bin/env python

import os
import subprocess
import sys
import pika
import json
import traceback
import functools
import threading

#------------------------------------------------------------------

# find and read the config file.
mypath = os.path.realpath (__file__)
mydir = os.path.dirname (mypath)
myconfig = mydir + "/cicd-config.py"
exec (compile (filename=myconfig, source=open (myconfig).read (), mode="exec"))

# Note that trybots need to use "trybot.var" for the above, due to
# scoping.

#------------------------------------------------------------------

trybot_callback = {}

# threaded workers from https://github.com/pika/pika/blob/master/examples/basic_consumer_threaded.py

def trybot_ack_cb (ch, delivery_tag, th):
    print ("in trybot_ack_cb");
    th.join();
    if ch.is_open:
        ch.basic_ack(delivery_tag)
    else:
        # Channel is already closed, so we can't ACK this message;
        # log and/or do something that makes sense for your app in this case.
        pass
    print ("exit trybot_ack_cb");

def trybot_worker_thread (conn, ch, delivery_tag, body_text):
    global saved_patch_id
    event = json.loads (body_text)
    print ("in trybot_worker_thread");

    # for dumb trybots, checks are applied to the last patch in a
    # series.
    try:
        if event['type'] == 'PATCH':
            for p in event['data']['patches']:
                saved_patch_id = p['id'];
    except:
        pass

    try:
        trybot_callback (event)
    except Exception as exc:
        print (traceback.format_exc())

    cb = functools.partial(trybot_ack_cb, ch, delivery_tag, threading.current_thread())
    conn.add_callback_threadsafe(cb)
    print ("exit trybot_worker_thread");

def local_callback(ch, method, properties, body_text, args):
    (conn, ) = args
    print ("in local_callback");

    delivery_tag = method.delivery_tag
    t = threading.Thread(target=trybot_worker_thread, args=(conn, ch, delivery_tag, body_text))
    t.start()
    print ("exit local_callback");


#------------------------------------------------------------------

def run(*args):
    args = list(args)
    proc = subprocess.Popen (args, stdout=subprocess.PIPE, close_fds=True, text=True, shell=False)
    fout = proc.stdout
    sout = fout.read ()
    fout.close ()
    rv = proc.wait ()
    return (rv, sout)

def runv(args):
    args = list(args)
    proc = subprocess.Popen (args, stdout=subprocess.PIPE, close_fds=True, text=True, shell=False)
    fout = proc.stdout
    sout = fout.read ()
    fout.close ()
    rv = proc.wait ()
    return (rv, sout)

#------------------------------------------------------------------

def start(myqueue, tb_callback):
    global trybot_callback;

    connection = pika.BlockingConnection(pika.ConnectionParameters(host = rabbitHost,
                                                                   port = rabbitPort))
    channel = connection.channel()

    channel.queue_declare(queue=myqueue, durable=True)

    trybot_callback = tb_callback;

    channel.basic_qos(prefetch_count=1)
    omcb = functools.partial(local_callback, args=(connection,))
    channel.basic_consume(queue=myqueue,
                          auto_ack=False,
                          on_message_callback=omcb)
    
    channel.start_consuming()
