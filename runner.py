#!/usr/bin/python3

def do_help():
    print ("""

Usage:
runner
""")    

# Read config from `dirname $0`.  Looks for any other runner_*.py
# scripts in the same directory.

#------------------------------------------------------------------

import os
import sys
import time
import datetime
import json
import mariadb
import requests
import pika

sys.stdout.reconfigure(line_buffering=True)

#------------------------------------------------------------------
# Generic timed-loop logic.  Pass min/max times and function to call.
# Function should return true if "something happened", which causes
# the poll time to be reset to minimum.

def do_timed_loop(minTime, maxTime, theFunction):
    curTime = minTime
    while True:
        if theFunction():
            curTime = minTime
        else:
            curTime = min (curTime * 2, maxTime)
        time.sleep (curTime)

#------------------------------------------------------------------
# Database connections

def open_database ():
    global db_conn
    global db_cur
    try:
        db_conn = mariadb.connect(
            user = runnerDBUser,
            password = runnerDBPassword,
            database = runnerDBName,
            host = '127.0.0.1')
    except mariadb.Error as err:
        print (err, file=sys.stderr)
        return False

    db_cur = db_conn.cursor ()
    return True

def close_database ():
    db_conn.close ()

# variables' values are strings

def db_get_variable (var):
    db_cur.execute("SELECT value FROM state WHERE variable = ?;", (var,))
    ret = db_cur.fetchall()
    # return the first field of the first row returned
    if len (ret) > 0:
        return ret[0][0]
    return False

def db_set_variable (var, value):
    db_cur.execute("insert into state values (?, ?) on duplicate key update value=?;",
                   (var, value, value))
    db_conn.commit()
    return False

#------------------------------------------------------------------

scripts = {};

# Scan $pwd for files like runner_*.pl and match them with a compiled
# dictionary "scripts"
def update_script_list ():
    seen = {}

    with os.scandir (mydir) as it:
        for entry in it:
            if entry.name.startswith('runner_') and entry.name.endswith('.py'):
                seen[entry.name] = 1
                filetime = entry.stat().st_mtime
                if entry.name in scripts:
                    savedtime = scripts[entry.name]['time'];
                else:
                    scripts[entry.name] = {}
                    savedtime = 0;
                if filetime != savedtime:
                    scripts[entry.name]['time'] = filetime;
                    try:
                        print("Compiling runner script ", entry.name)
                        scripts[entry.name]['code'] = compile (filename=entry.name,
                                                           source=open (entry.name).read (),
                                                           mode="exec")
                    except:
                        print ("Unable to compile", entry.name);
                        del scripts[entry.name]

    to_delete = {};
    for fname in scripts.keys():
        if not fname in seen:
            to_delete[fname] = 1
            print("delete:", fname);
    for fname in to_delete.keys():
        del scripts[fname];

def run_script_list ():
    for fname in scripts.keys():
        exec (scripts[fname]['code'])

#------------------------------------------------------------------

# Short for "maybe key", assumes global `event'
def mk(key):
    if key in event:
        return event[key];
    return None;

def do_event ():
    global event
    global rabbitmq_channel

    rabbitmq_channel = None;

    if not open_database ():
        return False
    got_a_new_event = False;

    now = datetime.datetime.utcnow()
    last_poll = db_get_variable ("last-runner-poll")
    if (last_poll == False):
        # The most we want the first time, depends on how old "expired" events are
        last_poll = now - datetime.timedelta (seconds = eventExpireTime)
    else:
        last_poll = datetime.datetime.fromisoformat (last_poll)

    q = {'since': last_poll.isoformat ()}

    ret_count = 0;
    new_count = 0;
    try:
        response = requests.get (curatorURL, params=q)
    except:
        # network error, don't reset timeouts
        return False;
    if response.status_code == 200:
        for event in response.json ():
            ret_count = ret_count + 1

            if event['type'] == 'PATCH':
                db_cur.execute("SELECT edate FROM runner WHERE type = 'PATCH' AND series_id = ?",
                               (event['series_id'],))

            elif event['type'] == 'COMMIT':
                db_cur.execute("SELECT edate FROM runner WHERE type = 'COMMIT' AND hash = ?",
                               (event['hash'],))

            elif event['type'] == 'COMMAND':
                db_cur.execute("SELECT edate FROM runner WHERE type = 'COMMAND' AND authorized_by = ? AND hash = ?",
                               (event['authorized_by'], event['hash']))

            else:
                print ("Unknown event type", event['type'])
                continue

            results = db_cur.fetchall ()
            if len (results) > 0:
                continue
            new_count = new_count + 1

            db_cur.execute("INSERT INTO runner VALUES (?, ?, ?, ?, ?, ?)",
                           (mk('date'), mk('type'), mk('series_id'),
                            mk('authorized_by'), mk('command'), mk('hash')));

            got_a_new_event = True

            if rabbitmq_channel == None:
                connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
                rabbitmq_channel = connection.channel()

            run_script_list ();

    if rabbitmq_channel != None:
        connection.close()

    expire_period = datetime.timedelta (seconds = eventExpireTime);
    expire_moment = now - expire_period;
    db_cur.execute("DELETE FROM runner WHERE edate < ?",
                   (expire_moment.isoformat(), ));

    db_conn.commit ()
    db_set_variable ("last-runner-poll", now.isoformat());
    close_database ()
    return got_a_new_event
    

def do_event_loop ():
    do_timed_loop (minCuratorPoll, maxCuratorPoll, do_event)

#------------------------------------------------------------------
# Callbacks

def add_tryjob (queue):
    global event
    print ("add_tryjob:", queue, event['type'])

    rabbitmq_channel.queue_declare (queue = queue, durable = True);

    rabbitmq_channel.basic_publish (exchange = '',
                                    routing_key = queue,
                                    body = json.dumps(event));

#------------------------------------------------------------------
# This is the main starting point for this file.

# find and read the config file.
mypath = os.path.realpath (__file__)
mydir = os.path.dirname (mypath)
myconfig = mydir + "/cicd-config.py"
exec (compile (filename=myconfig, source=open (myconfig).read (), mode="exec"))

update_script_list ()

do_event_loop ()
